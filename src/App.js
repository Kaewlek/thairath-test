import React, { useEffect, useState, useCallback } from 'react';
import './App.css';
import Card from './component/Card';
import { Col, Pagination, Row, Typography } from 'antd';
import Modal from 'antd/lib/modal/Modal';
import Img from './component/Img';

const initModal = {
  src: '',
  caption: '',
  gallery: [],
};
function App() {
  const [pagination, setPagination] = useState({
    page: 1,
    length: 10,
  });
  const [ig, setIg] = useState({
    record: 0,
    count: 0,
    data: [],
  });
  const [modal, setModal] = useState({
    visible: false,
    obj: initModal,
    imgActive: '',
  });

  const fetchData = useCallback(async () => {
    const response = await fetch(
      `http://localhost:5000/page/${pagination.page}/length/${pagination.length}`
    );
    const { result, record, count } = await response.json();
    await setIg({ data: result, record, count });
  }, [pagination]);

  const handleClick = (item) => {
    setModal({ ...modal, visible: true, obj: item, imgActive: item.src });
  };
  const handleClose = () => {
    setModal({ ...modal, visible: false, obj: initModal, imgActive: '' });
  };

  const handleChangeImg = (src) => {
    setModal({ ...modal, imgActive: src });
  };

  const handleChangePage = (page, length) => {
    setPagination({ ...pagination, page, length });
  };
  useEffect(() => {
    fetchData();
    return () => {};
  }, [fetchData]);

  return (
    <div className="App">
      <Typography.Title level={1} style={{ color: '#00b400' }}>
        ไทยรัฐ
      </Typography.Title>
      <Row>
        {ig.data.map((item, index) => (
          <Col key={index}>
            <Card
              onClick={() => handleClick(item)}
              content={item.caption}
              src={item.src}
            ></Card>
            <div></div>
          </Col>
        ))}
      </Row>
      <Row style={{ marginTop: 32 }} justify="center">
        <Col>
          <Pagination
            onChange={handleChangePage}
            current={pagination.page}
            total={ig.count}
            showSizeChanger
          />
        </Col>
      </Row>
      <Modal
        okButtonProps={{
          hidden: true,
        }}
        cancelButtonProps={{
          hidden: true,
        }}
        visible={modal.visible}
        onCancel={handleClose}
        footer={false}
        width={1100}
      >
        <Row>
          <Col md={12}>
            <Row justify="center">
              <Col>
                <Img maxWidth="100%" src={modal.imgActive} alt="news" />
              </Col>
            </Row>
            <Row>
              {modal.obj.gallery.map((item, index) => (
                <Col key={index} span={3}>
                  <Img
                    onClick={() => handleChangeImg(item.src)}
                    hover
                    maxHeight="64px"
                    src={item.src}
                  />
                </Col>
              ))}
            </Row>
          </Col>
          <Col xs={24} md={11}>
            <Typography.Paragraph
              style={{
                padding: 24,
                textIndent: 24,
              }}
            >
              {modal.obj.caption}
            </Typography.Paragraph>
          </Col>
        </Row>
      </Modal>
    </div>
  );
}

export default App;
