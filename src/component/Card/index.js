import React from 'react';
import { StyledCard, StyledImg } from './style';
export default function Card({ children, src, content, ...props }) {
  return (
    <StyledCard {...props}>
      <StyledImg src={src} />
    </StyledCard>
  );
}
