import styled from 'styled-components';
import { Card as CardAntd } from 'antd';
export const StyledCard = styled(CardAntd)`
  position: relative;
  border-radius: 0px;
  padding: 0px;
  margin: 0px;
`;

export const StyledImg = styled.img`
  height: 100vh;
  width: 100%;
  max-width: 300px;
  max-height: 300px;
  object-fit: cover;
  position: relative;
  cursor: pointer;
  overflow: hidden;
  :hover {
    transition: transform 500ms;
    transform: scale(1.3);
    z-index: 1;
  }
`;
