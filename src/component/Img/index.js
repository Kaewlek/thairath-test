import React from 'react';
import { StyledImg, StyledWrapper } from './style';
export default function Img({ src, maxWidth, maxHeight, hover, ...props }) {
  return (
    <StyledWrapper>
      <StyledImg
        {...props}
        maxWidth={maxWidth}
        maxHeight={maxHeight}
        hover={hover}
        src={src}
      ></StyledImg>
    </StyledWrapper>
  );
}
