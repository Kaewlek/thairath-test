import styled, { css } from 'styled-components';
export const StyledImg = styled.img`
  height: 100vh;
  width: 100%;
  max-width: ${({ maxWidth }) => maxWidth || '512px'};
  max-height: ${({ maxHeight }) => maxHeight || '512px'};
  object-fit: cover;
  position: relative;
  cursor: pointer;
  overflow: hidden;
  ${({ hover }) =>
    hover &&
    css`
      :hover {
        opacity: 0.7;
      }
    `}
`;
export const StyledWrapper = styled.div`
  display: flex;
`;
